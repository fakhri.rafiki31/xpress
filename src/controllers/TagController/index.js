import { Tag } from "../../models"

class TagController {
    static get = (req, res) =>
        Tag.findAll()
            .then((data) => res.status(200).json(data))
            .catch((err) => res.status(400).json({ message: err.message }))

    static create = (req, res) => {
        const { name } = req.body

        return Tag.create({ name })
            .then((data) => res.status(201).json(data))
            .catch((err) => res.status(400).json({ message: err.message }))
    }

    static update = async (req, res) => {
        const { id } = req.params

        try {
            const tag = await Tag.findOne({ where: { id } })

            if (!tag) return res.status(404).json({ message: "Failed to update. Tag not found" })

            const { name } = req.body
            await tag.update({ name })

            return res.status(200).json({ message: "success updated" })
        } catch (err) {
            res.status(400).json({ message: err.message })
        }
    }

    static delete = async (req, res) => {
        const { id } = req.params

        try {
            const tag = await Tag.destroy({ where: { id } })

            if (!tag) return res.status(404).json({ message: 'Failed to deleted. Tag not found' })

            return res.status(200).json({ message: 'Success to delete' })

        } catch (err) {
            res.status(400).json({ message: err.message })
        }
    }
}

export default TagController
