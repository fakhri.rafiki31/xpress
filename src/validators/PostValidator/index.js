import { body } from 'express-validator'

class PostValidator {
  static create = () => [
    body('id').exists().isInt({ gt: 0 }),
    body('title').exists().isString(),
    body('author').exists().isString(),
  ]
}

export default PostValidator
